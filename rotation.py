"""
Defines rotation matririces about the first, second, and third 
(x, y, z) axes in Euclidean space.

=INPUT=
    theta - float
        Desired rotation angle in radian
=OUTPUT=
    rotation_matrix - 3x3 ndArray
"""

import numpy as np


def x_matrix(theta):
    rotation_matrix = np.array([
        [1, 0, 0],
        [0, np.cos(theta), -np.sin(theta)],
        [0, np.sin(theta), np.cos(theta)]], dtype='float')
    return rotation_matrix


def y_matrix(theta):
    rotation_matrix = np.array([
        [np.cos(theta), 0, np.sin(theta)],
        [0, 1, 0],
        [np.sin(theta), 0, np.cos(theta)]], dtype='float')
    return rotation_matrix


def z_matrix(theta):
    rotation_matrix = np.array([
        [np.cos(theta), -np.sin(theta), 0],
        [np.sin(theta), np.cos(theta), 0],
        [0, 0, 1]], dtype='float')
    return rotation_matrix


def rotate_repeat(n_times, rotation_matrix, vector, steps=None):
    """
    Rotate input vector N times with given rotation matrix

    =INPUT=
        n_times - integer
            Number of times to repeat the rotation
        rotation_matrix - MxM ndArray
            Matrix to rotate with
        vector - Mx1 ndArray
        (steps) - list
            If provided, append the resulting vector of each intermediate
            step to the list.
    """
    
    result = vector
    for _ in range(0, n_times):
        result = rotation_matrix * result

    if type(steps) is list:
        steps.append(result)

    return result

