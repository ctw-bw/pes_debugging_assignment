"""
FIXME

Issues:
1) For some reason, step_results does not contain all intermediate
rotation steps

2) vector_rotated does not provide the expected result after 5 rotations.
It should be a 3x1 vector with:
[[0.67088835]
 [0.58870951]
 [0.35317127]]
"""

import numpy as np
import rotation as rot


# Initial vector and its desired rotation settings
vector = np.array([[0.5, 0.75, 0.33]], dtype='float').T
print(f'Vector has {vector.shape[0]} rows and {vector.shape[1]} columns')

theta_x = np.pi 
theta_y = 0.25 * np.pi
theta_z = 0.5 * np.pi
n_rotations = 5


# Matrix multiplication of rotation matrices to get a definitive rotation
zyx_matrix = (
    rot.z_matrix(theta_z) * rot.y_matrix(theta_y) * rot.x_matrix(theta_x))

# Rotate the vector N times and obtain intermediate step results.
step_results = []
vector_rotated = rot.rotate_repeat(
    n_rotations, zyx_matrix, vector, step_results)


print(vector_rotated)
print(len(step_results))

